# Splitter - Osíris
Repositório para hospedar os artefatos gerados durante o trabalho com os dados Splitter no projeto Osíris no AiLab.

## Análise sobre a Criação das Quatro Classes
Durante o trabalho com a utilização de quatro classes, foram feitas análises para o entendimento dessas funções.

Em essência o notebook [Data-Analysis](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/four-class-data-analysis/Data-Analysis.ipynb) mostra a diferente utilização e funcionamento dessas funções e como elas afetam os datasets.

No arquivo [pdf](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/four-class-data-analysis/Entendimento_4_Classes.pdf), encontra-se uma explicação mais textual e conceitual dessas funções.

Em síntese, o [splitter_four_class.csv](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/splitter_four_class.csv) é o resultado final da aplicação da função de quatro classes escolhida no projeto.

## Modelos
Estão sendo trabalhados dois modelos para a execução com quatro classes:
- [VGG16-ThreePage](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/models/VGG16-ThreePage.ipynb): Utilização do VGG16;
- [EFF-ThreePage](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/models/EFF-ThreePage.ipynb): Utilização do EFF.

Em [model](https://gitlab.com/victorleaoo/splitter-osiris/-/tree/main/models/src/model), encontra-se as Confusion Matrix e Final Report para as últimas execuções de cada um dos modelos.

## Dados em imagens
As imagens do splitter estão presentes na pasta [new_tif](https://gitlab.com/victorleaoo/splitter-osiris/-/tree/main/new_tif).

Elas se encontram no formato .tiff.

## Análise das Imagens com os Modelos
Para analisar as imagens com alguns labels de quatro classes DEPOIS da transformação, mas ANTES da classificação antes o modelo, o notebook [Images_Before_Train](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/models/Images_Before_Train.ipynb) foi criado.

Além disso, dentro do [EFF_ThreePage](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/models/EFF-ThreePage.ipynb) e do [VGG16-ThreePage](https://gitlab.com/victorleaoo/splitter-osiris/-/blob/main/models/VGG16-ThreePage.ipynb) foi feito o plot das imagens com as predições incorretas (presente no final do notebook).